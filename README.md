# day_2

Second day of the marathon in GoIT

Task:
1. Limit the ball so that it moves only in the middle of the playing field (left, right and upper boundaries of the field)
2. Input bonuses into the game (just like adding enemies), which should descend from top to bottom (not from right to left).